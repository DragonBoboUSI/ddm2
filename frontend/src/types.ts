export interface Person {
	vaccines: Array<Vaccine>;
	tests: Array<Test>;
}

export interface Vaccine {
	date: string;
	type: string;
	brand: string;
	location: string;
}

export interface Test {
	date: string;
	type: string;
	result: boolean | string;
	location: string;
}

export interface PersonInformation {
	id: string;
	name: string;
	surname: string;
	email: string;
	phone_number: string;
	address: string;
}
