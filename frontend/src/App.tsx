import React from 'react';
import FindPerson from 'views/FindPerson';
import DefectiveLot from 'views/DefectiveLot';
import Update from 'views/Update';
import NewTest from 'views/NewTest';
import NewVaccine from 'views/NewVaccine';
import Home from 'views/Home';
import { Route, Routes, BrowserRouter } from 'react-router-dom';
import { createGlobalStyle } from 'styled-components';

const App = () => (
	<BrowserRouter>
		<Routes>
			<Route element={<FindPerson />} path="/find" />
			<Route element={<DefectiveLot />} path="/defectivelot" />
			<Route element={<Update />} path="/update" />
			<Route element={<NewTest />} path="/update/test" />
			<Route element={<NewVaccine />} path="/update/vaccine" />
			<Route element={<Home />} path="/" />
		</Routes>
		<GlobalStyle />
	</BrowserRouter>
);

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
	font-family: Roboto, sans-serif;
  }
`;

export default App;
