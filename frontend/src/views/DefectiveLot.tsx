import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Typography } from '@material-ui/core';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { PersonInformation } from 'types';
import { Input } from 'Components';
import axios from 'axios';

type FormID = {
	id: string;
};

const DefectiveLot = () => {
	const { register: registerID, handleSubmit: handleSubmitID } =
		useForm<FormID>();

	const [response, setResponse] = useState<PersonInformation[]>();

	const [searched, setSearched] = useState(false);
	const [loading, setLoading] = useState(false);

	const onSubmitID = async ({ id }: FormID) => {
		try {
			if (id) {
				setSearched(false);
				setLoading(true);

				const axiosResponse = await axios.get(
					`http://localhost:4000/faulty-lot/${id}`,
				);
				const parsedData = axiosResponse.data;

				let parsedPersons = [];
				for (let i = 0; i < parsedData.length; i++) {
					parsedPersons.push({
						id: parsedData[i].id,
						name: parsedData[i].name,
						surname: parsedData[i].surname,
						email: parsedData[i].email,
						phone_number: parsedData[i].phone_number,
						address: parsedData[i].address,
					});
				}

				setResponse(parsedPersons);

				setLoading(false);
				setSearched(true);
			}
		} catch (e) {
			alert('Invalid data');
		}
	};

	const firstLine = () => {
		if (searched && response) {
			if (response.length > 0) {
				return (
					<StyledLine>
						<Typography variant="h5">
							<b>ID</b>
						</Typography>
						<Typography variant="h5">
							<b>Full Name</b>
						</Typography>
						<Typography variant="h5">
							<b>Email</b>
						</Typography>
						<Typography variant="h5">
							<b>Phone Number</b>
						</Typography>
						<Typography variant="h5">
							<b>Address</b>
						</Typography>
					</StyledLine>
				);
			}
		}
		return;
	};

	const show = () => {
		if (searched && response) {
			if (response.length > 0) {
				return response.map((person: PersonInformation) => (
					<StyledLine>
						<Typography variant="h5">{person.id}</Typography>
						<Typography variant="h5">
							{person.name} {person.surname}
						</Typography>
						<Typography variant="h5">{person.email}</Typography>
						<Typography variant="h5">
							{person.phone_number}
						</Typography>
						<Typography variant="h5">{person.address}</Typography>
					</StyledLine>
				));
			} else {
				return <Typography variant="h6">No persons found</Typography>;
			}
		}
		return;
	};

	const title = () => {
		if (searched && response) {
			if (response.length > 0) {
				return (
					<Typography variant="h4">
						<b>People to contact</b>
					</Typography>
				);
			}
		}
		return;
	};

	const NoResult = () => {
		if (searched && response) {
			if (response.length < 1) {
				return (
					<Typography variant="h6">
						<b>No results</b>
					</Typography>
				);
			}
		}
		return;
	};

	return (
		<MainPage>
			<Typography variant="h3">
				People with Defective Vaccine Lot
			</Typography>
			<StyledChoice>
				<FindForm onSubmit={handleSubmitID(onSubmitID)}>
					<Input {...registerID('id')} placeholder="Vaccine Lot ID" />
					<StyledSubmit type="submit" value="Search" />
				</FindForm>
			</StyledChoice>
			{loading ? (
				<FontAwesomeIcon icon={faSpinner} spin />
			) : (
				<Wrapper>
					{title()}
					{firstLine()}
					{show()}
					{NoResult()}
				</Wrapper>
			)}
			{searched && (response === null || response === undefined) ? (
				<Typography variant="h6">Something went wrong</Typography>
			) : (
				<></>
			)}
		</MainPage>
	);
};

const MainPage = styled.div`
	width: 100%;
	padding-top: 30px;
	display: flex;
	align-items: center;
	align-self: center;
	flex-direction: column;
`;

const StyledChoice = styled.div`
	width: 100%;
	padding-top: 30px;
	padding-bottom: 30px;
	display: flex;
	align-items: center;
	align-self: center;
	justify-content: center;
	align-content: space-around;
	flex-direction: column;
`;

const StyledLine = styled.div`
	display: grid;
	grid-template-columns: 100px 250px 350px 200px auto;
`;

const FindForm = styled.form`
	display: flex;
	align-items: center;
	align-self: center;
	flex-direction: column;
`;

const StyledSubmit = styled.input`
	margin: 7px;
	font-size: 1.2em;
`;

const Wrapper = styled.table``;

export default DefectiveLot;
