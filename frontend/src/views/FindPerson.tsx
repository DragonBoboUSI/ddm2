import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Typography } from '@material-ui/core';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { Person, Vaccine, Test } from 'types';
import { Input } from 'Components';
import axios from 'axios';

type FormID = {
	id: string;
};

const FindPerson = () => {
	const { register: registerID, handleSubmit: handleSubmitID } =
		useForm<FormID>();

	const [response, setResponse] = useState<Person>();
	const [filteredResponse, setFilteredResponse] = useState<Person>();
	const [checked, setChecked] = React.useState(false);

	const [searched, setSearched] = useState(false);
	const [loading, setLoading] = useState(false);

	const onSubmitID = async ({ id }: FormID) => {
		try {
			if (id) {
				setSearched(false);
				setLoading(true);

				const axiosResponse = await axios.get(
					`http://localhost:4000/person/${id}`,
				);
				const parsedData = axiosResponse.data;

				let parsedVaccination = [];
				let parsedFilteredVaccination = [];
				for (let i = 0; i < parsedData.vaccination.length; i++) {
					parsedVaccination.push({
						date: parsedData.vaccination[i].date,
						type: parsedData.vaccination[i].vaccine.type,
						brand: parsedData.vaccination[i].vaccine.brand,
						location: parsedData.vaccination[i].location.name,
					});
					if (
						monthDiff(
							new Date(),
							new Date(parsedData.vaccination[i].date),
						)
					) {
						parsedFilteredVaccination.push({
							date: parsedData.vaccination[i].date,
							type: parsedData.vaccination[i].vaccine.type,
							brand: parsedData.vaccination[i].vaccine.brand,
							location: parsedData.vaccination[i].location.name,
						});
					}
				}

				let parsedTests = [];
				let parsedFilteredTests = [];
				for (let i = 0; i < parsedData.tests.length; i++) {
					parsedTests.push({
						date: parsedData.tests[i].date,
						type: parsedData.tests[i].type,
						result: parsedData.tests[i].positive.toString(),
						location: parsedData.tests[i].location.name,
					});
					if (
						Math.abs(
							new Date().getTime() -
								new Date(parsedData.tests[i].date).getTime(),
						) /
							36e5 <=
						48
					) {
						parsedFilteredTests.push({
							date: parsedData.tests[i].date,
							type: parsedData.tests[i].type,
							result: parsedData.tests[i].positive.toString(),
							location: parsedData.tests[i].location.name,
						});
					}
				}

				setResponse({
					vaccines: parsedVaccination,
					tests: parsedTests,
				});
				setFilteredResponse({
					vaccines: parsedFilteredVaccination,
					tests: parsedFilteredTests,
				});

				setLoading(false);
				setSearched(true);
			}
		} catch (e) {
			alert('Invalid data');
		}
	};

	const monthDiff = (d1: Date, d2: Date) => {
		let months;
		months = (d2.getFullYear() - d1.getFullYear()) * 12;
		months -= d1.getMonth();
		months += d2.getMonth();
		return months <= 6;
	};

	const handleChange = () => {
		setChecked(!checked);
	};

	const firstLineVaccine = () => {
		if (searched && response) {
			if (response.tests.length > 0 || response.vaccines.length > 0) {
				return (
					<StyledLine>
						<Typography variant="h5">
							<b>Date</b>
						</Typography>
						<Typography variant="h5">
							<b>Type</b>
						</Typography>
						<Typography variant="h5">
							<b>Brand</b>
						</Typography>
						<Typography variant="h5">
							<b>Location</b>
						</Typography>
					</StyledLine>
				);
			}
		}
		return;
	};

	const firstLineTest = () => {
		if (searched && response) {
			if (response.tests.length > 0 || response.vaccines.length > 0) {
				return (
					<StyledLine>
						<Typography variant="h5">
							<b>Date</b>
						</Typography>
						<Typography variant="h5">
							<b>Type</b>
						</Typography>
						<Typography variant="h5">
							<b>Result</b>
						</Typography>
						<Typography variant="h5">
							<b>Location</b>
						</Typography>
					</StyledLine>
				);
			}
		}
		return;
	};

	const showVaccine = () => {
		if (searched && response) {
			if (response.vaccines.length > 0) {
				if (checked && filteredResponse) {
					if (filteredResponse.vaccines.length > 0) {
						return filteredResponse.vaccines.map(
							(vaccine: Vaccine) => (
								<StyledLine>
									<Typography variant="h5">
										{vaccine.date}
									</Typography>
									<Typography variant="h5">
										{vaccine.type}
									</Typography>
									<Typography variant="h5">
										{vaccine.brand}
									</Typography>
									<Typography variant="h5">
										{vaccine.location}
									</Typography>
								</StyledLine>
							),
						);
					}
					return (
						<Typography variant="h6">
							No vaccination found
						</Typography>
					);
				}
				return response.vaccines.map((vaccine: Vaccine) => (
					<StyledLine>
						<Typography variant="h5">{vaccine.date}</Typography>
						<Typography variant="h5">{vaccine.type}</Typography>
						<Typography variant="h5">{vaccine.brand}</Typography>
						<Typography variant="h5">{vaccine.location}</Typography>
					</StyledLine>
				));
			} else if (response.tests.length > 0) {
				return (
					<Typography variant="h6">No vaccination found</Typography>
				);
			}
		}
		return;
	};

	const showTest = () => {
		if (searched && response) {
			if (response.tests.length > 0) {
				if (checked && filteredResponse) {
					if (filteredResponse.tests.length > 0) {
						return filteredResponse.tests.map((test: Test) => (
							<StyledLine>
								<Typography variant="h5">
									{test.date}
								</Typography>
								<Typography variant="h5">
									{test.type}
								</Typography>
								<Typography variant="h5">
									{test.result}
								</Typography>
								<Typography variant="h5">
									{test.location}
								</Typography>
							</StyledLine>
						));
					}
					return <Typography variant="h6">No test found</Typography>;
				}
				return response.tests.map((test: Test) => (
					<StyledLine>
						<Typography variant="h5">{test.date}</Typography>
						<Typography variant="h5">{test.type}</Typography>
						<Typography variant="h5">{test.result}</Typography>
						<Typography variant="h5">{test.location}</Typography>
					</StyledLine>
				));
			} else if (response.vaccines.length > 0) {
				return <Typography variant="h6">No test found</Typography>;
			}
		}
		return;
	};

	const VaccineTitle = () => {
		if (searched && response) {
			if (response.tests.length > 0 || response.vaccines.length > 0) {
				if (checked) {
					return (
						<Typography variant="h4">
							<b>Currently Valid Vaccinations</b>
						</Typography>
					);
				}
				return (
					<Typography variant="h4">
						<b>Vaccinations</b>
					</Typography>
				);
			}
		}
		return;
	};

	const TestsTitle = () => {
		if (searched && response) {
			if (response.tests.length > 0 || response.vaccines.length > 0) {
				if (checked) {
					return (
						<Typography variant="h4">
							<b>Currently Valid Tests</b>
						</Typography>
					);
				}
				return (
					<Typography variant="h4">
						<b>Tests</b>
					</Typography>
				);
			}
		}
		return;
	};

	const NoResult = () => {
		if (searched && response) {
			if (response.tests.length < 1 && response.vaccines.length < 1) {
				return (
					<Typography variant="h6">
						<b>No tests or vaccinations performed</b>
					</Typography>
				);
			}
		}
		return;
	};

	const showSpace = () => {
		if (searched && response) {
			if (response.tests.length > 0 || response.vaccines.length > 0) {
				return <StyledSpace />;
			}
		}
		return;
	};

	return (
		<MainPage>
			<Typography variant="h3">Find Application</Typography>
			<StyledChoice>
				<FindForm onSubmit={handleSubmitID(onSubmitID)}>
					<Input {...registerID('id')} placeholder="Person ID" />
					<StyledSubmit type="submit" value="Search" />
				</FindForm>
				<StyledLabel>
					<input
						type="checkbox"
						checked={checked}
						onChange={handleChange}
					/>
					Show only currently valid <br />
					(vaccination: 6 months, test: 48 hours)
				</StyledLabel>
			</StyledChoice>
			{loading ? (
				<FontAwesomeIcon icon={faSpinner} spin />
			) : (
				<Wrapper>
					{VaccineTitle()}
					{firstLineVaccine()}
					{showVaccine()}
					{showSpace()}
					{TestsTitle()}
					{firstLineTest()}
					{showTest()}
					{NoResult()}
				</Wrapper>
			)}
			{searched && (response === null || response === undefined) ? (
				<Typography variant="h6">Something went wrong</Typography>
			) : (
				<></>
			)}
		</MainPage>
	);
};

const MainPage = styled.div`
	width: 100%;
	padding-top: 30px;
	display: flex;
	align-items: center;
	align-self: center;
	flex-direction: column;
`;

const StyledChoice = styled.div`
	width: 100%;
	padding-top: 30px;
	padding-bottom: 30px;
	display: flex;
	align-items: center;
	align-self: center;
	justify-content: center;
	align-content: space-around;
	flex-direction: column;
`;

const StyledLine = styled.div`
	display: grid;
	grid-template-columns: 350px 150px 150px 300px auto;
`;

const FindForm = styled.form`
	display: flex;
	align-items: center;
	align-self: center;
	flex-direction: column;
`;

const StyledSpace = styled.div`
	height: 25px;
`;

const StyledSubmit = styled.input`
	margin: 7px;
	font-size: 1.2em;
`;

const StyledLabel = styled.label`
	text-align: center;
`;

const Wrapper = styled.table``;

export default FindPerson;
