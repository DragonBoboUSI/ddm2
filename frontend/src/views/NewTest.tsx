import { useState } from 'react';
import React from 'react';
import axios from 'axios';
import { getDateFromString } from 'utils';
import { useForm } from 'react-hook-form';
import { Input } from 'Components';
import styled from 'styled-components';
import { Typography } from '@material-ui/core';

type FormTest = {
	id: string;
	location: string;
	testPositive: 'true' | 'false';
	date: string;
	type: 'PCR' | 'antiGen';
};

const NewTest = () => {
	const { register, handleSubmit } = useForm<FormTest>({
		defaultValues: {
			testPositive: 'false',
			type: 'PCR',
		},
	});

	const [status, setStatus] = useState(-1);

	const onSubmit = async ({
		id,
		location,
		testPositive,
		date,
		type,
	}: FormTest) => {
		setStatus(-1);
		try {
			const parsedDate = getDateFromString(date).getTime();

			const axiosResponse = await axios.post(
				`http://localhost:4000/test`,
				{
					personId: id,
					locationName: location,
					type: type,
					date: parsedDate,
					positive: testPositive,
				},
			);

			setStatus(axiosResponse.status);
		} catch (e) {
			setStatus(0);
		}
	};

	const feedback = () => {
		if (status === 200) {
			return <Typography variant="h6">OK</Typography>;
		} else if (status !== -1) {
			return <Typography variant="h6">Failed</Typography>;
		} else {
			return <></>;
		}
	};

	return (
		<MainPage>
			<Typography variant="h3">Add new Test</Typography>
			<TracingForm onSubmit={handleSubmit(onSubmit)}>
				<Input {...register('id')} placeholder="Person ID" />
				<Input
					{...register('date')}
					placeholder="Date (YYYY:MM:DD:HH:mm)"
				/>
				<StyledFormLine>
					<select {...register('type')}>
						<option>PCR</option>
						<option>antiGen</option>
					</select>
				</StyledFormLine>
				<StyledFormLine>
					<select {...register('testPositive')}>
						<option>true</option>
						<option>false</option>
					</select>
				</StyledFormLine>
				<Input {...register('location')} placeholder="Location Name" />
				<StyledSubmit type="submit" value="Set" />
			</TracingForm>
			{feedback()}
		</MainPage>
	);
};

const MainPage = styled.div`
	width: 100%;
	padding-top: 30px;
	display: flex;
	align-items: center;
	align-self: center;
	flex-direction: column;
`;

const TracingForm = styled.form`
	display: flex;
	align-items: center;
	align-self: center;
	flex-direction: column;
	padding-top: 30px;
`;

const StyledSubmit = styled.input`
	margin: 7px;
	font-size: 1.2em;
`;

const StyledFormLine = styled.div`
	display: flex;
	padding: 10px;
	width: 300px;
	justify-content: space-around;
`;

export default NewTest;
