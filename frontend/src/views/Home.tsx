import React, { FC } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

type MenuOption = {
	name: string;
	path: string;
	color?: string;
	hoverColor?: string;
};

const menu: MenuOption[] = [
	{
		name: 'Find Person',
		path: 'find',
	},
	{
		name: 'Update',
		path: 'update',
	},
	{
		name: 'Defective Lot',
		path: 'defectivelot',
	},
];

const Home: FC = () => (
	<StyledHome>
		<h1>{`Data & Design Modeling Project`}</h1>
		<h3>{`Certification App`}</h3>
		<StyledMenuContainer>
			{menu.map(
				({
					name,
					path,
					color = 'rgb(0, 0, 165)',
					hoverColor = 'rgb(155, 235, 255)',
				}) => (
					<StyledLink
						to={path}
						color={color}
						hovercolor={hoverColor}
						key={path}
					>
						{name}
					</StyledLink>
				),
			)}
		</StyledMenuContainer>
	</StyledHome>
);

const StyledHome = styled.div`
	display: flex;
	flex-direction: column;
	width: 100vw;
	height: 100vh;
	padding-top: 30px;
	align-items: center;
`;

const StyledMenuContainer = styled.div`
	display: grid;
	grid-template-columns: 1fr 1fr 1fr;
`;

const StyledLink = styled(Link)<{ color: string; hovercolor: string }>`
	display: grid;
	padding: 20px 30px;
	margin: 10px;
	width: 150px;
	text-align: center;
	height: min-content;
	border-radius: 20px;
	background-color: ${({ color }) => color};
	color: white;
	text-decoration: none;
	transition: all 0.25s;
	font-size: 1.2em;
	&:hover {
		background-color: ${({ hovercolor }) => hovercolor};
		color: black;
	}
`;

export default Home;
