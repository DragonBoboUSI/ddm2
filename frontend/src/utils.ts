import { Person } from "types";

export const parseData = (
    data: { meta: { id: number }; row: Person[] | any }[],
) => {
    const newData: Person[] = [];
    data.forEach((item, index) => {
        if (item.meta.id !== 0 && "id" in item.row[0]) {
            newData.push(item.row[0]);
        }
    });
    return newData;
};

export const getDateFromString = (date: string) => {
    let newDateArgs = date.split(":").map((el) => parseInt(el));
    const newDate = new Date(
        newDateArgs[0],
        newDateArgs[1] - 1,
        newDateArgs[2],
        newDateArgs[3],
        newDateArgs[4],
    );

    return newDate;
};
