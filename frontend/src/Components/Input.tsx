import React, { InputHTMLAttributes, forwardRef } from 'react';
import styled from 'styled-components';

const Input = forwardRef<
	HTMLInputElement,
	InputHTMLAttributes<HTMLInputElement>
>((props, ref) => <StyledInput {...props} ref={ref} />);

const StyledInput = styled.input`
	width: 200px;
	font-size: 1.2em;
	margin: 5px;
`;

export default Input;
