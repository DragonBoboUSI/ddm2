import express, { json } from "express";
import { collections, connectToDatabase } from "./services/database.service";
import { Person } from "models/Person";
import { WithId } from "mongodb";
import {
    createLocationsDb,
    createPersonelDb,
    createPersonsContacts,
    createPersonsDb,
    createTestsDb,
    createVaccination,
    createVaccinationsDb,
    createVaccinsDb,
    getPerson,
} from "./helper";
import { Location } from "models/Location";
import { Personel, ReturnPersonel } from "models/Personel";
import { Vaccine } from "models/Vaccine";
import { Test } from "models/Test";
import { PERSONEL_NUMBER } from "./constants";

const app = express();

const PORT = process.env.PORT || 4000;

app.get("/generate-db", async (req, res) => {
    await createVaccinsDb();
    console.log(`Created vaccines`);
    await createLocationsDb();
    console.log(`Created locations`);
    await createPersonsDb();
    console.log(`Created persons`);
    await createPersonsContacts();
    console.log(`Added persons contact`);
    await createPersonelDb();
    console.log(`Created personel`);
    await createVaccinationsDb();
    console.log(`Created vaccinations`);
    await createTestsDb();
    console.log(`Created tests`);
    console.log(`Database successfully created`);
});

app.get("/person/:id", async (req, res) => {
    await connectToDatabase();

    const { id } = req.params;

    const person = (await collections.person?.findOne({
        id: parseInt(id),
    })) as WithId<Person>;

    const getFullPerson = await getPerson(person._id);

    res.json(getFullPerson);
});

app.get("/locations/:size", async (req, res) => {
    await connectToDatabase();

    const { size = 10 } = req.params;

    const locations = (await collections.location
        ?.aggregate([
            {
                $sample: {
                    size,
                },
            },
        ])
        .toArray()) as WithId<Location>[];

    res.json(locations);
});

app.get("/personel/:size", async (req, res) => {
    await connectToDatabase();

    const { size = 10 } = req.params;

    const personels = (await collections.personel
        ?.aggregate([
            {
                $sample: {
                    size,
                },
            },
        ])
        .toArray()) as WithId<Personel>[];

    const returnPersonel: ReturnPersonel[] = [];

    for (const personel of personels) {
        const person = (await collections.person?.findOne({
            id: personel.person,
        })) as WithId<Person>;

        returnPersonel.push({
            ...personel,
            person,
        });
    }

    res.json(returnPersonel);
});

app.post("/vaccines", json(), async (req, res) => {
    await connectToDatabase();

    const { type, brand, size = 1 } = req.body;

    const vaccines = (await collections.vaccines
        ?.aggregate([
            {
                $match: {
                    type,
                },
            },
            {
                $match: {
                    brand,
                },
            },
            {
                $sample: {
                    size,
                },
            },
        ])
        .toArray()) as Vaccine[];

    res.json(vaccines);
});

app.post("/vaccination", json(), async (req, res) => {
    await connectToDatabase();

    const { personId, locationName, brand, type, date } = req.body;

    const person = (await collections.person?.findOne({
        id: parseInt(personId),
    })) as WithId<Person>;

    const vaccine = (
        (await collections.vaccines
            ?.aggregate([
                {
                    $match: {
                        type,
                    },
                },
                {
                    $match: {
                        brand,
                    },
                },
                {
                    $sample: {
                        size: 1,
                    },
                },
            ])
            .toArray()) as WithId<Vaccine>[]
    )[0];

    const location = (
        (await collections.location
            ?.aggregate([
                {
                    $match: {
                        name: locationName,
                    },
                },
                {
                    $sample: {
                        size: 1,
                    },
                },
            ])
            .toArray()) as WithId<Location>[]
    )[0];

    const newVaccination = await createVaccination(location, date, vaccine);

    const newVaccinationResult = await collections.vaccines?.insertOne(
        newVaccination,
    );

    if (newVaccinationResult?.insertedId) {
        person.vaccination = [
            ...person.vaccination,
            newVaccinationResult?.insertedId,
        ];
        await collections.person?.findOneAndReplace(
            { _id: person._id },
            person,
        );
    }

    res.json(newVaccination);
});

app.post("/test", json(), async (req, res) => {
    await connectToDatabase();

    const {
        personId,
        locationName,
        type,
        date = new Date(),
        positive,
    } = req.body;

    const person = (await collections.person?.findOne({
        id: parseInt(personId),
    })) as WithId<Person>;

    const location = (
        (await collections.location
            ?.aggregate([
                {
                    $match: {
                        name: locationName,
                    },
                },
                {
                    $sample: {
                        size: 1,
                    },
                },
            ])
            .toArray()) as WithId<Location>[]
    )[0];

    let personel = (await collections.personel?.findOne({
        id: Math.ceil(Math.random() * PERSONEL_NUMBER),
    })) as WithId<Personel>;

    const newTest: Test = {
        personel: [personel._id],
        location: location._id,
        date,
        type,
        positive,
    };

    const newTestResult = await collections.test?.insertOne(newTest);

    if (newTestResult?.insertedId) {
        person.tests = [...person.tests, newTestResult?.insertedId];
        await collections.person?.findOneAndReplace(
            { _id: person._id },
            person,
        );
    }

    res.json(newTestResult);
});

app.get("/faulty-lot/:id", async (req, res) => {
    await connectToDatabase();

    const { id: lot_id } = req.params;

    const asd = await collections.person
        ?.aggregate([
            {
                $lookup: {
                    from: "Vaccination",
                    localField: "vaccination",
                    foreignField: "_id",
                    as: "vaccination",
                },
            },
            {
                $unwind: "$vaccination",
            },
            {
                $lookup: {
                    from: "Vaccines",
                    localField: "vaccination.vaccine",
                    foreignField: "_id",
                    as: "vaccination.vaccine",
                },
            },
            {
                $match: {
                    "vaccination.vaccine.lot": lot_id,
                },
            },
        ])
        .toArray();

    res.json(asd);
});

app.listen(PORT, () =>
    console.log(`⚡Server is running here 👉 https://localhost:${PORT}`),
);
