import express from "express";
import { Vaccine } from "models/Vaccine";
import { readFileSync } from "fs";
import { addDays, addMonths, subDays, subMonths } from "date-fns";

import { collections, connectToDatabase } from "./services/database.service";
import { Person, ReturnPerson } from "models/Person";
import { Personel } from "models/Personel";
import { Location } from "models/Location";
import { ReturnVaccination, Vaccination } from "models/Vaccination";
import { ObjectId, WithId } from "mongodb";
import {
    LOCATION_NUMBER,
    PERSONEL_NUMBER,
    PERSONS_NUMBER,
    VACCINE_NUMBER,
} from "./constants";
import { TestType, Test, ReturnTest } from "models/Test";

export const createVaccinsDb = async () => {
    await connectToDatabase();
    const file = readFileSync(__dirname + "/db-files/Vaccine.json", "utf8");

    const parsedFile = JSON.parse(file);

    const toAdd: Vaccine[] = [];

    for (const data of parsedFile) {
        toAdd.push({
            id: data.id,
            production_date: new Date(data.production_date),
            lot: data.lot,
            type: data.type,
            brand: data.brand,
        });
    }

    await collections.vaccines?.insertMany(toAdd);
};

export const createPersonsDb = async () => {
    const file = readFileSync(__dirname + "/db-files/Person.json", "utf8");

    const parsedFile = JSON.parse(file);

    const toAdd: Person[] = [];

    for (const data of parsedFile) {
        toAdd.push({
            id: data.id,
            name: data.name,
            surname: data.surname,
            email: data.email,
            phone_number: data.phone_number,
            address: data.address,
            vaccination: [],
            tests: [],
            contact_persons: [],
            date_of_birth: new Date(data.date_of_birth),
            sex: data.sex,
            medical_notes: data.medical_notes,
            nationality: data.nationality,
            canton: data.canton,
        });
    }

    await collections.person?.insertMany(toAdd);
};

export const createPersonelDb = async () => {
    const file = readFileSync(__dirname + "/db-files/Personel.json", "utf8");

    const parsedFile = JSON.parse(file);

    const toAdd: Personel[] = [];

    for (const data of parsedFile) {
        const person = await collections.person?.findOne({
            id: data.id,
        });

        if (person) {
            toAdd.push({
                id: data.id,
                title: data.title,
                person: person._id,
            });
        }
    }

    await collections.personel?.insertMany(toAdd);
};

export const createLocationsDb = async () => {
    const file = readFileSync(__dirname + "/db-files/Location.json", "utf8");

    const parsedFile = JSON.parse(file) as Location[];

    const toAdd: Location[] = [];

    for (const data of parsedFile) {
        toAdd.push({
            id: data.id,
            latitude: data.latitude,
            longitude: data.longitude,
            name: data.name,
            department: data.department,
            phone_number: data.phone_number,
            email: data.email,
        });
    }

    await collections.location?.insertMany(toAdd);
};

export const createVaccinationsDb = async () => {
    const persons = (await collections.person?.find({}).toArray()) as Person[];

    for (const person of persons) {
        const check = Math.random();
        const vaccinations: Vaccination[] = [];
        if (check < 0.1) {
            const location = (await collections.location?.findOne({
                id: Math.ceil(Math.random() * LOCATION_NUMBER),
            })) as WithId<Location>;

            const vaccination1Date = randomDate(
                subDays(subMonths(new Date(), 7), 5),
                addDays(subMonths(new Date(), 7), 5),
            );

            const vaccination1 = await createVaccination(
                location,
                vaccination1Date,
            );

            const vaccine = (await collections.vaccines?.findOne({
                _id: vaccination1._id,
            })) as WithId<Vaccine>;

            const vaccination2Date = randomDate(
                subDays(subMonths(new Date(), 6), 5),
                addDays(subMonths(new Date(), 6), 5),
            );

            const vaccination2 = await createVaccination(
                location,
                vaccination2Date,
                vaccine,
            );

            const vaccination3Date = randomDate(
                subMonths(new Date(), 1),
                new Date(),
            );

            const vaccination3 = await createVaccination(
                location,
                vaccination3Date,
                vaccine,
            );

            vaccinations.push(vaccination1, vaccination2, vaccination3);
        } else if (check < 0.7) {
            const location = (await collections.location?.findOne({
                id: Math.ceil(Math.random() * LOCATION_NUMBER),
            })) as WithId<Location>;

            const vaccination1Date = randomDate(
                new Date("2020/12/25"),
                subMonths(new Date(), 2),
            );

            const vaccination1 = await createVaccination(
                location,
                vaccination1Date,
            );

            const vaccine = (await collections.vaccines?.findOne({
                _id: vaccination1._id,
            })) as WithId<Vaccine>;

            const vaccination2Date = randomDate(
                subDays(addMonths(vaccination1Date, 1), 5),
                addDays(addMonths(vaccination1Date, 1), 5),
            );

            const vaccination2 = await createVaccination(
                location,
                vaccination2Date,
                vaccine,
            );

            vaccinations.push(vaccination1, vaccination2);
        } else if (check < 0.8) {
            const location = (await collections.location?.findOne({
                id: Math.ceil(Math.random() * LOCATION_NUMBER),
            })) as WithId<Location>;

            const vaccination1Date = randomDate(
                subMonths(new Date(), 1),
                new Date(),
            );

            const vaccination1 = await createVaccination(
                location,
                vaccination1Date,
            );
            vaccinations.push(vaccination1);
        }
        if (vaccinations.length > 0) {
            const result = await collections.vaccination?.insertMany(
                vaccinations,
            );
            const vaccinationsId = Object.values(result?.insertedIds ?? {});
            person.vaccination = vaccinationsId;
            await collections.person?.findOneAndReplace(
                { _id: person._id },
                person,
            );
        }
    }
};

export const createTestsDb = async () => {
    const persons = (await collections.person?.find({}).toArray()) as Person[];

    for (const person of persons) {
        const numberOfTest = Math.floor(Math.random() * 10);

        const tests: Test[] = [];
        for (const x of Array.from(Array(numberOfTest).keys())) {
            const test = await createTest();
            tests.push(test);
        }

        if (tests.length > 0) {
            const result = await collections.test?.insertMany(tests);
            const testsId = Object.values(result?.insertedIds ?? {});
            person.tests = testsId;
            await collections.person?.findOneAndReplace(
                { _id: person._id },
                person,
            );
        }
    }
};

export const createPersonsContacts = async () => {
    const persons = (await collections.person?.find({}).toArray()) as Person[];

    for (const person of persons) {
        const numberOfPeople = Math.floor(Math.random() * 3);

        const emergencyContacts: ObjectId[] = [];
        for (const x of Array.from(Array(numberOfPeople).keys())) {
            const emergencyContact = (await collections.person?.findOne({
                id: Math.ceil(Math.random() * PERSONS_NUMBER),
            })) as WithId<Person>;
            if (emergencyContact.id !== person.id) {
                emergencyContacts.push(emergencyContact._id);
            }
        }

        if (emergencyContacts.length > 0) {
            person.contact_persons = emergencyContacts;
            await collections.person?.findOneAndReplace(
                { _id: person._id },
                person,
            );
        }
    }
};

/* Utils */
export const randomDate = (start: Date, end: Date) => {
    return new Date(
        start.getTime() + Math.random() * (end.getTime() - start.getTime()),
    );
};

export const createVaccination = async (
    location: WithId<Location>,
    date: Date,
    vaccine?: WithId<Vaccine>,
) => {
    const check = Math.random();
    let personel1;

    if (check < 0.3) {
        personel1 = (
            (await collections.personel
                ?.aggregate([
                    {
                        $match: {
                            title: "Nurse",
                        },
                    },
                    {
                        $sample: {
                            size: 1,
                        },
                    },
                ])
                .toArray()) as any[]
        )[0];
    } else if (check < 0.6) {
        personel1 = (
            (await collections.personel
                ?.aggregate([
                    {
                        $match: {
                            title: "Doctor",
                        },
                    },
                    {
                        $sample: {
                            size: 1,
                        },
                    },
                ])
                .toArray()) as any[]
        )[0];
    } else {
        personel1 = (
            (await collections.personel
                ?.aggregate([
                    {
                        $match: {
                            title: "Health Operator",
                        },
                    },
                    {
                        $sample: {
                            size: 1,
                        },
                    },
                ])
                .toArray()) as any[]
        )[0];
    }

    const personel2 = (
        (await collections.personel
            ?.aggregate([
                {
                    $match: {
                        title: "Assistant",
                    },
                },
                {
                    $sample: {
                        size: 1,
                    },
                },
            ])
            .toArray()) as any[]
    )[0];

    let newVaccine;
    if (vaccine) {
        newVaccine = (
            (await collections.vaccines
                ?.aggregate([
                    {
                        $match: {
                            brand: vaccine.brand,
                        },
                    },
                    {
                        $sample: {
                            size: 1,
                        },
                    },
                ])
                .toArray()) as any[]
        )[0];
    } else {
        newVaccine = (await collections.vaccines?.findOne({
            id: Math.ceil(Math.random() * VACCINE_NUMBER),
        })) as Vaccine;
    }

    const newVaccination: Vaccination = {
        vaccine: newVaccine._id,
        date,
        location: location._id,
        personel: [personel1._id, personel2._id],
    };

    return newVaccination;
};

export const createTest = async () => {
    let personel = (await collections.personel?.findOne({
        id: Math.ceil(Math.random() * PERSONEL_NUMBER),
    })) as WithId<Personel>;

    let location = (await collections.location?.findOne({
        id: Math.ceil(Math.random() * LOCATION_NUMBER),
    })) as WithId<Location>;

    let result = false;
    let type: TestType = "Antigen";

    if (Math.random() < 0.1) {
        result = true;
    }

    if (Math.random() < 0.3) {
        type = "PCR";
    }

    const date = randomDate(new Date("2020/03/1"), new Date());

    const newTest: Test = {
        location: location._id,
        personel: [personel._id],
        type,
        date,
        positive: result,
    };

    return newTest;
};

export const createTestWithInfo = async (
    personelIds: ObjectId[],
    locationId: ObjectId,
    type: TestType,
    result: boolean,
    date: Date,
) => {
    const newTest: Test = {
        location: locationId,
        personel: personelIds,
        type,
        date,
        positive: result,
    };

    return newTest;
};

export const getPerson = async (id: ObjectId) => {
    const person = (await collections.person?.findOne({
        _id: id,
    })) as Person;

    const vaccinationIds = person.vaccination;
    const vaccinations = [];

    for (const vacc of vaccinationIds) {
        const vaccination = await getVaccination(vacc);
        vaccinations.push(vaccination);
    }

    const testIds = person.tests;
    const tests = [];

    for (const te of testIds) {
        const test = await getTest(te);
        tests.push(test);
    }

    const contactIds = person.contact_persons;
    const contacts = [];

    for (const te of contactIds) {
        const contact = (await collections.person?.findOne({
            _id: id,
        })) as Person;
        contacts.push(contact);
    }

    const fullPerson: ReturnPerson = {
        ...person,
        vaccination: vaccinations,
        tests: tests,
        contact_persons: contacts,
    };

    return fullPerson;
};

export const getVaccination = async (id: ObjectId) => {
    const vaccination = (await collections.vaccination?.findOne({
        _id: id,
    })) as Vaccination;

    const personelIds = vaccination.personel;
    const personels = [];

    for (const te of personelIds) {
        const personel = (await collections.personel?.findOne({
            _id: te,
        })) as Personel;
        personels.push(personel);
    }

    const location = (await collections.location?.findOne({
        _id: vaccination.location,
    })) as Location;

    const vaccine = (await collections.vaccines?.findOne({
        _id: vaccination.vaccine,
    })) as Vaccine;

    const fullVaccination: ReturnVaccination = {
        ...vaccination,
        personel: personels,
        location,
        vaccine,
    };

    return fullVaccination;
};

export const getTest = async (id: ObjectId) => {
    const test = (await collections.test?.findOne({
        _id: id,
    })) as Test;

    const personelIds = test.personel;
    const personels = [];

    for (const te of personelIds) {
        const personel = (await collections.personel?.findOne({
            _id: te,
        })) as Personel;
        personels.push(personel);
    }

    const location = (await collections.location?.findOne({
        _id: test.location,
    })) as Location;

    const fullTest: ReturnTest = {
        ...test,
        personel: personels,
        location,
    };

    return fullTest;
};
