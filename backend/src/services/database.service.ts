// External Dependencies
import { Vaccination } from "models/Vaccination";
import * as mongoDB from "mongodb";
import * as dotenv from "dotenv";

dotenv.config({ path: __dirname + "/../../.env" });
// Global Variables
export const collections: {
    vaccines?: mongoDB.Collection;
    person?: mongoDB.Collection;
    personel?: mongoDB.Collection;
    location?: mongoDB.Collection;
    vaccination?: mongoDB.Collection<Vaccination>;
    test?: mongoDB.Collection;
} = {};

// Initialize Connection
export async function connectToDatabase() {
    const mongoDbConnectionUrl = process.env.MONGO_DB_CONNECTION_URL ?? "";
    const client: mongoDB.MongoClient = new mongoDB.MongoClient(
        mongoDbConnectionUrl,
    );

    await client.connect();

    const db: mongoDB.Db = client.db("DDM2");

    const vaccinesCollection: mongoDB.Collection = db.collection("Vaccines");
    const personCollection: mongoDB.Collection = db.collection("Person");
    const personelCollection: mongoDB.Collection = db.collection("Personel");
    const locationCollection: mongoDB.Collection = db.collection("Location");
    const testCollection: mongoDB.Collection = db.collection("Test");
    const vaccinationCollection: mongoDB.Collection<Vaccination> =
        db.collection("Vaccination");

    collections.vaccines = vaccinesCollection;
    collections.person = personCollection;
    collections.personel = personelCollection;
    collections.location = locationCollection;
    collections.vaccination = vaccinationCollection;
    collections.test = testCollection;
    console.log(`Successfully connected to database: ${db.databaseName}`);
}
