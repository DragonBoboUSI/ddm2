import { ObjectId } from "mongodb";

export type VaccineType = "mRNA" | "vector";
export type VaccineBrand = "Moderna" | "Pfizer" | "J&J";

export interface Vaccine {
    _id?: ObjectId;
    id: number;
    production_date: Date;
    type: VaccineType;
    brand: VaccineBrand;
    lot: string;
}
