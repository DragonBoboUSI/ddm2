import { ObjectId } from "mongodb";
import { Location } from "./Location";
import { Personel } from "./Personel";
import { Vaccine } from "./Vaccine";

export interface Vaccination {
    _id?: ObjectId;
    location: ObjectId;
    date: Date;
    personel: ObjectId[];
    vaccine: ObjectId;
}

export interface ReturnVaccination {
    _id?: ObjectId;
    location: Location;
    date: Date;
    personel: Personel[];
    vaccine: Vaccine;
}
