import { ObjectId } from "mongodb";
import { Location } from "./Location";
import { Personel } from "./Personel";

export type TestType = "PCR" | "Antigen";

export interface Test {
    _id?: ObjectId;
    location: ObjectId;
    date: Date;
    type: TestType;
    personel: ObjectId[];
    positive: boolean;
}

export interface ReturnTest {
    _id?: ObjectId;
    location: Location;
    date: Date;
    type: TestType;
    personel: Personel[];
    positive: boolean;
}
