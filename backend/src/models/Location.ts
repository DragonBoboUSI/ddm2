import { ObjectId } from "mongodb";

export type Department =
    | "hospital"
    | "vaccination center"
    | "medical office"
    | "pharmacy";

export interface Location {
    _id?: ObjectId;
    id: number;
    latitude: number;
    longitude: number;
    name: string;
    department: Department;
    phone_number: string;
    email: string;
}
