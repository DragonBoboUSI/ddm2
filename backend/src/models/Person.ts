import { ObjectId } from "mongodb";
import { ReturnTest, Test } from "./Test";
import { ReturnVaccination, Vaccination } from "./Vaccination";

type Canton =
    | "ZH"
    | "BE"
    | "LU"
    | "UR"
    | "SZ"
    | "OW"
    | "NW"
    | "GL"
    | "ZG"
    | "FR"
    | "SO"
    | "BS"
    | "BL"
    | "SH"
    | "AR"
    | "AI"
    | "SG"
    | "GR"
    | "AG"
    | "TG"
    | "TI"
    | "VD"
    | "VS"
    | "NE"
    | "GE"
    | "JU";
type Nationality =
    | "Switzerland"
    | "Italy"
    | "Germany"
    | "Spain"
    | "French"
    | "Austria"
    | "Poland";

export interface Person {
    _id?: ObjectId;
    id: number;
    name: string;
    surname: string;
    email: string;
    phone_number: string;
    address: string;
    vaccination: ObjectId[];
    tests: ObjectId[];
    contact_persons: ObjectId[];
    date_of_birth: Date;
    sex: string;
    medical_notes: string;
    nationality: Nationality;
    canton: Canton;
}

export interface ReturnPerson {
    _id?: ObjectId;
    id: number;
    name: string;
    surname: string;
    email: string;
    phone_number: string;
    address: string;
    vaccination: ReturnVaccination[];
    tests: ReturnTest[];
    contact_persons: Person[];
    date_of_birth: Date;
    sex: string;
    medical_notes: string;
    nationality: Nationality;
    canton: Canton;
}
