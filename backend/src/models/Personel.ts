import { ObjectId } from "mongodb";
import { Person } from "./Person";

export type PersonelTitle =
    | "Doctor"
    | "Nurse"
    | "Health Operator"
    | "Assistant";

export interface Personel {
    _id?: ObjectId;
    id: number;
    title: PersonelTitle;
    person: ObjectId;
}

export interface ReturnPersonel {
    _id?: ObjectId;
    id: number;
    title: PersonelTitle;
    person: Person;
}
