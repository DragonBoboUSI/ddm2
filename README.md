#DDM Assignment 2 GUI part

## Introduction

For this assignment we used MongoDb Atlas.
In order to setup the db, it is necessary to obtain a mongodb connection string.

## Preparation

1. Pull the repository
2. Navigate to the root of the project (where this README is contained) and run `npm run install`
3. After the install process is finished, navigate to the backend folder and create a .env file. This file must include `MONGO_DB_CONNECTION_URL=<connection-url>`
4. Navigate back to the root of the project and run `npm start`
5. To create the database, open a browser to the page `localhost:4000/generate-db`. This process might take some time, it is divided in 7 steps which should be logged in the console
6. After the process is complete, open a browser on `localhost:3000` and enjoy

#### For questions or problems, contact us
